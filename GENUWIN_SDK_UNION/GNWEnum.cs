﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GENUWIN_SDK_UNION
{
    [Flags]
    public enum CMD_TYPE: int
    {
        CMD_DOWNLOAD_USER_TO_TERMINAL = 1,

        /// <summary>
        /// Delete 1 user 
        /// lstuser: 1 lstterminal :1++
        /// </summary>
        CMD_DELETE_USER_TO_TERMINAL = 2,
        /// <summary>
        /// Update user từ terminal về server
        /// lstuser: 1 lstterminal:1
        /// </summary>
        CMD_LOAD_USER_FROM_TERMINAL_TO_SERVER = 3,
        /// <summary>
        /// load list user từ terminal(chỉ load 1 máy) 
        /// 1 user : lstuser=1++ lstterminal=1
        /// </summary>
        CMD_LOAD_LIST_USER_INFO_FROM_TERMINAL_TO_SERVER = 4,
        /// <summary>
        /// Delete tất cả user
        /// chưa test đc
        /// </summary>
        CMD_DELETE_ALL_USER_ON_TERMINAL = 5,
        /// <summary>
        /// load list user từ terminal(chỉ load 1 máy)       
        /// load all: lstuser=null lstterminal=1
        /// </summary>
        CMD_LOAD_ALL_USER_INFO_FROM_TERMINAL = 6,
        CMD_DOWNLOAD_PEROID_LOG = 7,
        CMD_TERMINAL_ADD = 8,
 
        CMD_DOWNLOAD_NEW_LOG = 9,
        CMD_DOWNLOAD_OLD_LOG = 10,
        CMD_DOWNLOAD_ALL_LOG = 11,
        CMD_COUNT_LOG = 12,

        CMD_START_REQUEST = 93,
        CMD_SET_TIME = 94,
        CMD_READ_SYSCONFIG_INFO = 95,

        CMD_READCARD_ON_TERMINAL = 96,
        CMD_REGISTER_FACE_ON_TERMINAL = 97,
        CMD_COUNT_USER_IN_TERMINAL = 98,
        CMD_REGISTER_FINGER_ON_TERMINAL = 99,

        CMD_UNKNOW = 100
    }

    public enum DEVICE_STATUS
    {
        //0~100 CAC TRANG THAI CONNECT THANH CONG CUA THIET BI
        AVAILABLE = 0,

        AVAILABLE_CONNECTING = 4,
        AVAILABLE_CONNECTED = 5,
        AVAILABLE_MONITORING = 6,
        AVAILABLE_MONITOR = 7,

        DEVICE_CHANGED_INFO = 100,

        //ERROR  > 1000 

        //LOI KET NOI - CO THE CONNECT LAI - > 10000
        CONNECT_DEVICE_ERROR = 10001,
        DEVICE_FOUND_ERROR = 10002,
        DISCONNECTED = 10003,

        //CAC LOI VE THONG TIN VA API - KHONG CONNECT - > 99000 
        PORT_FORMAT_ERROR = 99001,
        TYPE_FORMAT_ERROR = 99002,
        IPADDRESS_ERROR_1 = 99003,
        IPADDRESS_ERROR_2 = 99004,
        IPADDRESS_ERROR_3 = 99005,
        IPADDRESS_ERROR_4 = 99006,

        LOAD_API_ERROR = 99007,
        LOAD_API_CONTEXT_ERROR = 99008,
        LOAD_API_INITIALIZA_ERROR = 99009,
        LOAD_API_INITIALIZA_LISTENER_ERROR = 99010,

        SERVER_STOPED = 99900,
        NOT_SUPPORT = 99999,
        DEVICE_REMOVED = 99998
    }
    public enum DEVICE_MESAGES
    {

        DEVICE_STATUS_CHANGED = 10,
        DEVICE_LOG_EVENT = 11,
        DATA_BACK = 12,
        DOOR_ID = 0,

        ZONE_ID = 1,
        IO_INFO = 2,
        USER_ID = 3,
        USER_ID_AND_TNAKEY = 4,
        GENERA = 5
    }
}
