﻿using GENUWIN_SDK_UNION.Device;
using System.Windows.Forms;

namespace GENUWIN_SDK_UNION.Classes
{
    public static class ThreadHelperClass
    {
        private delegate void SetTextCallback(Form f, Control ctrl, string text);

        private delegate void SetLogCallback(Form f, ListView ctrl, string[] log, string tag);

        private delegate ListViewItem GetItemCallback(Form f, ListView ctrl, string dev_id);

        /// <summary>
        /// Set text property of various controls
        /// </summary>
        /// <param name="form">The calling form</param>
        /// <param name="ctrl"></param>
        /// <param name="text"></param>
        public static void SetText(Form form, Control ctrl, string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (ctrl.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                form.Invoke(d, new object[] { form, ctrl, text });
            }
            else
            {
                ctrl.Text = text;
            }
        }

        public static void SetLog(Form form, ListView ctrl, string[] log, string tag)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (ctrl.InvokeRequired)
            {
                SetLogCallback d = new SetLogCallback(SetLog);
                form.Invoke(d, new object[] { form, ctrl, log, tag });
            }
            else
            {
                bool isExists = false;
                ListViewItem item = new ListViewItem(log);
                item.Tag = tag;

                foreach (ListViewItem i in ctrl.Items)
                {
                    if (i.Tag + "" == tag)
                    {
                        isExists = true;
                        break;
                    }
                }
                if (!isExists)
                {
                    ctrl.Items.Insert(0, item);
                }
            }
        }

        public static ListViewItem GetItem(Form form, ListView ctrl, string dev_id)
        {
            ListViewItem rtnItem = null;

            if (ctrl.InvokeRequired)
            {
                GetItemCallback d = new GetItemCallback(GetItem);
                rtnItem = (ListViewItem)form.Invoke(d, new object[] { form, ctrl, dev_id });
            }
            else
            {

                foreach (ListViewItem item2 in ctrl.Items)
                {
                    if (dev_id.Equals(((BaseDevice)item2.Tag).Deviceinfo.Dev_Id))
                    {
                        rtnItem = item2;
                        break;
                    }
                }
                return rtnItem;
            }

            return rtnItem;
        }
    }
}