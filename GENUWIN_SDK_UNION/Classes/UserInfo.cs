﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GENUWIN_SDK_UNION.Classes
{
    public class UserInfo
    {
        public BasicInfo basicInfo { get; set; }

        public Properties properties { get; set; }

        public AccessAuthority accessAuthority { get; set; }

        public RFID rfid { get; set; }

        public Password password { get; set; }

        public FingerPrint fingerPrint { get; set; }

        public AccessFlag accessflag { get; set; }

        public int AuthType { get; set; }



        public class BasicInfo
        {
            public string userID { get; set; }
            public string uniqueID { get; set; }
            public string name { get; set; }
        }
        public class Properties
        {
            public bool isAdmin { get; set; }
            public bool isIdentify { get; set; }
            public bool isAndOperation { get; set; }
            public bool isCard { get; set; }
            public bool IsIris { get; set; }
            public bool isPassword { get; set; }
            public bool isFPCard { get; set; }
            public bool isFingerprint { get; set; }
            public bool isFace { get; set; }
        }
        public class AccessFlag
        {
            public bool isBlacklist { get; set; }
            public bool isFace1toN { get; set; }
            public bool isIris1toN { get; set; }
            public bool isExceptPassback { get; set; }
        }
        public class AccessAuthority
        {
            public string groupCode { get; set; }
            public string dateType { get; set; }
            public DateTime startDate { get; set; }
            public DateTime endtDate { get; set; }
        }
        public class RFID
        {
            public string rfid { get; set; }
        }
        public class Password
        {
            public string password { get; set; }
        }
        public class FingerPrint
        {
            public int securityLevel { get; set; }
        }
    }
}
