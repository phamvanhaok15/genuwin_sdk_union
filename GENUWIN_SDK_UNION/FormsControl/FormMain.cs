﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using UCSAPICOMLib;
using UCBioBSPCOMLib;
using System.Threading;
using System.IO;
using static GENUWIN_SDK_UNION.Device.BaseDevice;
using GENUWIN_SDK_UNION.Device;
using GENUWIN_SDK_UNION.Classes;

namespace GENUWIN_SDK_UNION
{
    public partial class FormMain : Form
    {
        private DatabaseHelper ora = null;
        public delegate void delegateAddListViewItem(DeviceInfo device);
        public delegate void delegateModifyListViewItem(ListViewItem item);
        public bool isrunning = false;
        public UCSAPICOMLib.UCSAPI ucsAPI;
        public FormMain()
        {
            InitializeComponent();
            InitDatabase();

            timerLoadTerminal.Interval = 1000 * 10;
            timerLoadTerminal.Start();

            ucsAPI = new UCSAPIClass();
            ucsAPI.ServerStart(255, int.Parse(GNWUtil.GetDefault().ApplicationPort.ToString()));
            if (ucsAPI.ErrorCode.ToString("X4") != "0000")
            {
                Console.WriteLine("Cannot open port " + int.Parse(GNWUtil.GetDefault().ApplicationPort.ToString()));
            }
            else
            {
                Console.WriteLine("open port " + int.Parse(GNWUtil.GetDefault().ApplicationPort.ToString()));
            }    
            //LoadTerminal();
        }

        private void OnStartConnect()
        {
            foreach (ListViewItem item2 in listviewDevice.Items)
            {
                ((BaseDevice)item2.Tag).StartConnect();
            }
        }

        private void OnStopConnect()
        {
            foreach (ListViewItem item2 in listviewDevice.Items)
            {
                ((BaseDevice)item2.Tag).StopConneted(true);
            }
        }

        private void LoadTerminal()
        {
            if (ora != null && ora.checkConnect())
            {
                try
                {
                    string pro = "hr_sel_terminal_list_union_v2";
                    DataTable dt = ora.excuteProcedure(pro, new List<string> { GNWUtil.GetDefault().CompanyPK, GNWUtil.GetDefault().Branch });
                    if (dt != null)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            DeviceInfo dev = new DeviceInfo(dt.Rows[i]["id_ter"].ToString()
                                                , dt.Rows[i]["name_ter"].ToString()
                                                , dt.Rows[i]["TYPE_TER"].ToString()
                                                , dt.Rows[i]["ip_ter"].ToString()
                                                , dt.Rows[i]["port_ter"].ToString()
                                                , dt.Rows[i]["group_ter"].ToString()
                                                , dt.Rows[i]["code_ter"].ToString()
                                                );
                            Console.WriteLine(dt.Rows[i]["name_ter"].ToString());
                            DrawGridTerminal(dev);
                        }
                    }
                }
                catch (Exception e)
                {

                    throw e;
                }
            }
        }

        private void DrawGridTerminal(DeviceInfo dev)
        {
            try
            {
                ListViewItem item = null;

                //foreach (ListViewItem item2 in listviewDevice.Items)
                //{
                //    if (dev.Dev_Id.Equals(((BaseDevice)item2.Tag).Deviceinfo.Dev_Id))
                //    {
                //        item = item2;
                //        break;
                //    }
                //}
                item = ThreadHelperClass.GetItem(this, listviewDevice, dev.Dev_Id);
                if (item != null)//không trùng
                {
                    listviewDevice.Invoke(new delegateModifyListViewItem(ModifyListViewItem), item);

                }
                else
                {
                    listviewDevice.Invoke(new delegateAddListViewItem(AddListViewItem), dev);
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            
        }
        public void AddListViewItem(DeviceInfo device)
        {
            ListViewItem item = new ListViewItem(device.Dev_Id + " - " + device.Dev_Name);
            item.SubItems.Add(device.Dev_Code);
            item.SubItems.Add(device.Dev_Ip + ":" + device.Dev_Port);
            item.SubItems.Add("");
            item.SubItems.Add(device.Dev_Group);


            BaseDevice baseDevice = BaseDevice.CreateInstance(device);
            baseDevice.logEvent += LogEvent;
            baseDevice.StartConnect();

            item.Tag = baseDevice;

            if (this.listviewDevice.InvokeRequired)
            {
                this.listviewDevice.BeginInvoke((MethodInvoker)delegate () { listviewDevice.Items.Add(item); });
            }
            else
            {
                listviewDevice.Items.Add(item);
            }

            ListViewGroup lstGroup = listviewDevice.Groups[baseDevice.Deviceinfo.Dev_Group];

            if (lstGroup == null)
            {
                lstGroup = new ListViewGroup(baseDevice.Deviceinfo.Dev_Group, baseDevice.Deviceinfo.Dev_Group);

                if (this.listviewDevice.InvokeRequired)
                {
                    this.listviewDevice.BeginInvoke((MethodInvoker)delegate ()
                    {
                        listviewDevice.Groups.Add(lstGroup);
                    });
                }
                else
                {
                    listviewDevice.Groups.Add(lstGroup); ;
                }
            }
            item.Group = lstGroup;

            SetInfotoItem(item);
        }
        public void ModifyListViewItem(ListViewItem item)
        {
            SetInfotoItem(item);
        }
        public void SetInfotoItem(ListViewItem item)
        {
            BaseDevice baseDevice = (BaseDevice)item.Tag;
            Color iColor = Color.Yellow;
            String imgKey = "device_on";
            if (baseDevice.Status == DEVICE_STATUS.NOT_SUPPORT)
            {
                imgKey = "device_unknown";
                iColor = Color.Yellow;
            }
            else
            {
                int ivalue = (int)baseDevice.Status;

                if (ivalue <= 100)
                {
                    imgKey = "device_on";
                    iColor = Color.DarkSeaGreen;
                }
                else if (ivalue > 1000)
                {
                    imgKey = "device_off";
                    iColor = Color.Red;
                }
                else if (ivalue == 1000)
                {

                }
                item.ImageKey = imgKey;

                if (listviewDevice.View == View.Details)
                {
                    item.BackColor = iColor;
                }
                else
                {
                    item.BackColor = Color.Yellow;
                }

                item.SubItems[3].Text = baseDevice.StatusMessages;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Config config = new Config();
            config.ShowDialog();
            MessageBox.Show("If you change config.You must restart application.");

        }

        public void InitDatabase()
        {
            DatabaseConfig dbConfig = new DatabaseConfig();
            string configFile = Directory.GetCurrentDirectory() + "\\db.config";
            if (File.Exists(configFile))
            {
                dbConfig.loadConfig(configFile);
            }
            try
            {
                ora = new DatabaseHelper(dbConfig);
                //MessageBox.Show("success");
            }
            catch (Exception a)
            {
                // MessageBox.Show("fail"+a.Message);
                //throw a;
            }


        }

        private void timerLoadTerminal_Tick(object sender, EventArgs e)
        {
            if (!isrunning)
            {
                isrunning = true;
                new Thread(AutoStart).Start();
            }
        }
        private void AutoStart()
        {

            //Thread.Sleep(4000);
            //load list terminal
            LoadTerminal();

            isrunning = false;

        }
        public void LogEvent(DEVICE_MESAGES type, String deviceID, String timespan, DEVICE_STATUS status, String mesages, String id, Boolean image, String data, Object obj)
        {
            switch (type)
            {
                case DEVICE_MESAGES.DEVICE_STATUS_CHANGED:
                    ListViewItem item = null;

                    //for (int j = 0; j < listviewDevice.Items.Count; j++)
                    //{
                    //    if (deviceID.Equals(((BaseDevice)listviewDevice.Items[j].Tag).Deviceinfo.Dev_Id))
                    //    {
                    //        item = listviewDevice.Items[j];
                    //        break;
                    //    }
                    //}
                    item = ThreadHelperClass.GetItem(this, listviewDevice, deviceID);

                    if (item == null)
                    {
                        if (status == DEVICE_STATUS.AVAILABLE)
                        {
                            // Invoke(new delegateAddListViewItem(AddListViewItem), deviceID, status, arg3);
                        }
                    }
                    else
                    {
                        listviewDevice.Invoke(new delegateModifyListViewItem(ModifyListViewItem), item);
                        //upd database status
                    }
                    break;
                default:
                    ListViewItem item3 = new ListViewItem(deviceID);
                    item3.SubItems.Add(id);
                    item3.SubItems.Add(timespan.ToString());
                    item3.SubItems.Add(data);
                    //listviewLog.DoubleBuffering(true);
                    if (this.listviewLog.InvokeRequired)
                    {
                        this.listviewLog.Invoke((MethodInvoker)delegate () { listviewLog.Items.Insert(0, item3); });
                    }
                    else
                    {
                        listviewLog.Items.Insert(0, item3);
                    }
                    if (listviewLog.Items.Count > 1000)
                    {
                        listviewLog.Items.RemoveAt(listviewLog.Items.Count - 1);
                    }
                    //update real access log
                    break;
            }
        }
    }
}
