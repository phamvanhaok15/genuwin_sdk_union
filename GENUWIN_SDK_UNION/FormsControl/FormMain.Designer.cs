﻿namespace GENUWIN_SDK_UNION
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listviewLog = new System.Windows.Forms.ListView();
            this.Terminal_id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.User_id = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Time = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Event_log = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Image = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.listviewDevice = new System.Windows.Forms.ListView();
            this.Terminal_id_1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Code_1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Terminal_IP_1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Status_1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Group_1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timerLoadTerminal = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listviewDevice);
            this.groupBox1.Location = new System.Drawing.Point(10, 94);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(835, 740);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Terminal Info";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listviewLog);
            this.groupBox2.Location = new System.Drawing.Point(852, 94);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(899, 740);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Log Info";
            // 
            // listviewLog
            // 
            this.listviewLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Terminal_id,
            this.User_id,
            this.Time,
            this.Event_log,
            this.Image});
            this.listviewLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listviewLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listviewLog.FullRowSelect = true;
            this.listviewLog.GridLines = true;
            this.listviewLog.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listviewLog.HideSelection = false;
            this.listviewLog.LabelEdit = true;
            this.listviewLog.Location = new System.Drawing.Point(3, 20);
            this.listviewLog.Name = "listviewLog";
            this.listviewLog.Size = new System.Drawing.Size(893, 717);
            this.listviewLog.TabIndex = 0;
            this.listviewLog.UseCompatibleStateImageBehavior = false;
            this.listviewLog.View = System.Windows.Forms.View.Details;
            // 
            // Terminal_id
            // 
            this.Terminal_id.Text = "Terminal ID";
            this.Terminal_id.Width = 160;
            // 
            // User_id
            // 
            this.User_id.Text = "User Id";
            this.User_id.Width = 160;
            // 
            // Time
            // 
            this.Time.Text = "Time";
            this.Time.Width = 160;
            // 
            // Event_log
            // 
            this.Event_log.Text = "Event Log";
            this.Event_log.Width = 300;
            // 
            // Image
            // 
            this.Image.Text = "Image";
            this.Image.Width = 100;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 31);
            this.button1.TabIndex = 2;
            this.button1.Text = "Config";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listviewDevice
            // 
            this.listviewDevice.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Terminal_id_1,
            this.Code_1,
            this.Terminal_IP_1,
            this.Status_1,
            this.Group_1});
            this.listviewDevice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listviewDevice.HideSelection = false;
            this.listviewDevice.Location = new System.Drawing.Point(3, 20);
            this.listviewDevice.Name = "listviewDevice";
            this.listviewDevice.Size = new System.Drawing.Size(829, 717);
            this.listviewDevice.TabIndex = 0;
            this.listviewDevice.UseCompatibleStateImageBehavior = false;
            this.listviewDevice.View = System.Windows.Forms.View.Details;
            // 
            // Terminal_id_1
            // 
            this.Terminal_id_1.Text = "Terminal ID";
            this.Terminal_id_1.Width = 200;
            // 
            // Code_1
            // 
            this.Code_1.Text = "Code";
            // 
            // Terminal_IP_1
            // 
            this.Terminal_IP_1.Text = "Terminal IP";
            this.Terminal_IP_1.Width = 200;
            // 
            // Status_1
            // 
            this.Status_1.Text = "Status";
            this.Status_1.Width = 160;
            // 
            // Group_1
            // 
            this.Group_1.Text = "Group";
            this.Group_1.Width = 200;
            // 
            // timerLoadTerminal
            // 
            this.timerLoadTerminal.Tick += new System.EventHandler(this.timerLoadTerminal_Tick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1757, 866);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormMain";
            this.Text = "GENUWIN SDK UNION";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView listviewLog;
        private System.Windows.Forms.ColumnHeader Terminal_id;
        private System.Windows.Forms.ColumnHeader User_id;
        private System.Windows.Forms.ColumnHeader Time;
        private System.Windows.Forms.ColumnHeader Event_log;
        private System.Windows.Forms.ColumnHeader Image;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView listviewDevice;
        private System.Windows.Forms.ColumnHeader Terminal_id_1;
        private System.Windows.Forms.ColumnHeader Code_1;
        private System.Windows.Forms.ColumnHeader Terminal_IP_1;
        private System.Windows.Forms.ColumnHeader Status_1;
        private System.Windows.Forms.ColumnHeader Group_1;
        private System.Windows.Forms.Timer timerLoadTerminal;
    }
}

