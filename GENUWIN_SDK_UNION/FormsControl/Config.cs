﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.IO;
namespace GENUWIN_SDK_UNION
{
    public partial class Config : Form
    {
        private DatabaseHelper ora = null;

        //string file_name = "";
        public string company_pk = "";
        public string branch_code = "";
        public string serverIP = "";
        public string serverUser = "";
        public string databaseName = "";
        public string serverPass = "";
        public string databsePort = "";
        public string aplicationPort = "";
        public Config()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void txtServerIP_TextChanged(object sender, EventArgs e)
        {

        }
        public void SaveConfig()
        {
            try
            {
                DatabaseConfig config = getConfigFromForm();
                if (ora != null)
                {
                    ora.dbConfig = config;
                }

                config.saveConfig();
                GENUWIN_SDK_UNION.Properties.Settings.Default.CompanyPK = config.company_pk;
                GENUWIN_SDK_UNION.Properties.Settings.Default.Branch = config.branch;
                GENUWIN_SDK_UNION.Properties.Settings.Default.ApplicationPort = config.appPort;
            }
            catch (Exception)
            {

                throw;
            }
            MessageBox.Show("Save success");
        }
        public void ReadConfig()
        {
            DatabaseConfig dbConfig = new DatabaseConfig();
            string configFile = Directory.GetCurrentDirectory() + "\\db.config";
            if (File.Exists(configFile))
            {
                dbConfig.loadConfig(configFile);

                txtServerIP.Text = dbConfig.host;
                txtDBPort.Text = dbConfig.port;
                txtDataName.Text = dbConfig.servicename;
                txtUser.Text = dbConfig.username;
                txtPass.Text = dbConfig.password;
                txtAPPort.Text = dbConfig.appPort;
                txtCompany.Text = dbConfig.company_pk;
                txtBranch.Text = dbConfig.branch;
            }
             
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void Config_Load(object sender, EventArgs e)
        {
            ReadConfig();
        }
        public bool checkConfig()
        {
            return true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            SaveConfig();
            this.Close();
        }

        private void txtAPPort_TextChanged(object sender, EventArgs e)
        {

        }

        private DatabaseConfig getConfigFromForm()
        {
            var _dbConfig = new DatabaseConfig();
            _dbConfig.host = txtServerIP.Text;
            _dbConfig.port = txtDBPort.Text;
            _dbConfig.servicename = txtDataName.Text;
            _dbConfig.username = txtUser.Text;
            _dbConfig.password = txtPass.Text;
            _dbConfig.appPort = txtAPPort.Text;
            _dbConfig.company_pk = txtCompany.Text;
            _dbConfig.branch = txtBranch.Text;
            //try { _dbConfig.loadCheckScript(txtCheckScript.Text); } catch { }
            //try { _dbConfig.loadTransferScript(txtTransferScript.Text); } catch { }

            return _dbConfig;
        }

    }
}
