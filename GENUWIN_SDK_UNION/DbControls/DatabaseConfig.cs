﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GENUWIN_SDK_UNION
{
    public class DatabaseConfig
    {
        public string host;
        public string port = "1521";
        public string servicename;
        public string username;
        public string password;
        public string appPort = "9870";
        public string company_pk;
        public string branch;

        public string checkScript = "";
        public string transferScript = "";
        public string tranferProcedure = "";

        public DatabaseConfig()
        {
        }

        public DatabaseConfig(string configPath)
        {
        }

        public string toString()
        {
            string rtn = "";

            rtn += "host:" + host;
            rtn += ";port:" + port;
            rtn += ";servicename:" + servicename;
            rtn += ";username:" + username;
            rtn += ";password:" + password;
            rtn += ";appPort:" + appPort;
            rtn += ";company_pk:" + company_pk;
            rtn += ";branch:" + branch;
            return rtn;
        }

        public void saveConfig()
        {
            try
            {
                string config = toString();
                string encryptStr = EncryptionManagement.Encrypt(config, true);

                string savePath = Directory.GetCurrentDirectory() + "\\db.config";
                if (File.Exists(savePath))
                {
                    File.Delete(savePath);
                }

                using (FileStream fs = File.Create(savePath))
                {
                    Byte[] savebytes = new UTF8Encoding(true).GetBytes(encryptStr);
                    fs.Write(savebytes, 0, savebytes.Length);
                    fs.Close();
                    fs.Dispose();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void loadConfig(string filePath)
        {
            try
            {
                using (StreamReader sr = File.OpenText(filePath))
                {
                    string encryptStr = sr.ReadToEnd();
                    string decryptStr = EncryptionManagement.Decrypt(encryptStr, true);

                    List<string> infos = decryptStr.Split(';').ToList();
                    foreach (var info in infos)
                    {
                        string[] data = info.Split(':');

                        string value = data.Length == 2 ? data[1] : "";
                        switch (data[0])
                        {
                            case "host": host = value; break;
                            case "port": port = value; break;
                            case "servicename": servicename = value; break;
                            case "username": username = value; break;
                            case "password": password = value; break;
                            case "appPort": appPort = value; break;
                            case "company_pk": company_pk = value; break;
                            case "branch": branch = value; break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void loadCheckScript(string filePath)
        {
            try
            {
                using (StreamReader sr = File.OpenText(filePath))
                {
                    string script = sr.ReadToEnd();
                    checkScript = script.Replace("\r", " ").Replace("\n", " ").Replace("\t", " ");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void loadTransferScript(string filePath)
        {
            try
            {
                using (StreamReader sr = File.OpenText(filePath))
                {
                    string script = sr.ReadToEnd();
                    transferScript = script.Replace("\r", " ").Replace("\n", " ").Replace("\t", " ").ToUpper();
                    try
                    {
                        tranferProcedure =
                            transferScript.Substring(
                                transferScript.IndexOf("PROCEDURE"),
                                transferScript.IndexOf("(") - transferScript.IndexOf("PROCEDURE")
                            ).Replace("PROCEDURE", "").Trim();
                    }
                    catch { }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}