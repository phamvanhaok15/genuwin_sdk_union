﻿using GENUWIN_SDK_UNION.DeviceControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GENUWIN_SDK_UNION.Device
{
    public abstract class  BaseDevice
    {
        protected DeviceInfo deviceInfo;
        protected DEVICE_STATUS status = DEVICE_STATUS.NOT_SUPPORT;
        public string DeviceName = "Device";
        public int Connect = 0;
        protected System.Windows.Forms.Timer timer = null;
        Action<object> action = (object obj) =>
        {
            Console.WriteLine("Task={0}, obj={1}, Thread={2}",
            Task.CurrentId, obj,
            Thread.CurrentThread.ManagedThreadId);
        };
        public struct DeviceInfo
        {
            public string Dev_Id;
            public string Dev_Name;
            public string Dev_Type;
            public string Dev_Ip;
            public string Dev_Port;
            public string Dev_Group;
            public string Dev_Code;

            public DeviceInfo(string dev_Id, string dev_Name, string dev_Type, string dev_Ip, string dev_Port, string dev_Group, string dev_Code)
            {
                Dev_Id = dev_Id;
                Dev_Name = dev_Name;
                Dev_Type = dev_Type;
                Dev_Ip = dev_Ip;
                Dev_Port = dev_Port;
                Dev_Group = dev_Group;
                Dev_Code = dev_Code;
            }
        }

        public DeviceInfo Deviceinfo { get => deviceInfo; protected set => deviceInfo = value; }

        protected BaseDevice(DeviceInfo device)
        {
            this.deviceInfo = device;
            timer = new System.Windows.Forms.Timer();

            Random r = new Random();
            timer.Interval = 40000 + r.Next(10000, 20000);
            timer.Enabled = true;
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        public void DeviceLog(string msg)
        {
            Console.WriteLine(deviceInfo.Dev_Ip + " : " + msg);
        }

        public static BaseDevice CreateInstance(DeviceInfo device)
        {
            try
            {
                int iType = Int32.Parse(device.Dev_Type);
                switch (iType)
                {
                    case 13:
                        return new UbioXpro2Device(device);
                    default:
                        return new UnKnowDevice(device);
                }
            }
            catch
            {
                return new UnKnowDevice(device);
            }
        }
        /*------------------------------------------------------*/
        public delegate void LogEvent(DEVICE_MESAGES type, String deviceID, String timespan, DEVICE_STATUS status, String mesages, String id, Boolean image, String data, Object obj);
        public event LogEvent logEvent;
        protected void RaiseLogEvent(String timespan, String id, Boolean image, String data, string v)
        {

            LogEvent evt = logEvent;
            if (evt != null)
            {
                string statusMessages = StatusMessages;
                evt(DEVICE_MESAGES.DEVICE_LOG_EVENT, deviceInfo.Dev_Id, timespan, Status, statusMessages, id, image, data, v);
            }
        }
        /*------------------------------------------------------*/

        public DEVICE_STATUS Status
        {
            get => status;
            set
            {
                if (status != value)
                {
                    status = value;

                    switch (status)
                    {
                        case DEVICE_STATUS.AVAILABLE_CONNECTED:
                            Connect = 1;
                            break;
                        default:
                            Connect = 0;
                            break;
                    }
                    RaiseStatusChange(status, StatusMessages);
                }
            }
        }
        protected void RaiseStatusChange(DEVICE_STATUS status, string mesages)
        {
            LogEvent evt = logEvent;
            if (evt != null)
            {
                DateTime myDate1 = new DateTime(1970, 1, 9, 0, 0, 00);
                DateTime myDate2 = DateTime.Now;

                TimeSpan myDateResult;

                myDateResult = myDate2 - myDate1;

                evt(DEVICE_MESAGES.DEVICE_STATUS_CHANGED, deviceInfo.Dev_Id, myDateResult.ToString(), status, mesages, "", false, "", null);
            }
        }
        public string StatusMessages
        {
            get
            {
                String sStatus = "Unknow";
                switch (status)
                {
                    //0~100 CAC TRANG THAI CONNECT THANH CONG CUA THIET BI
                    case DEVICE_STATUS.DISCONNECTED:
                        sStatus = "Disconnected";
                        break;
                    case DEVICE_STATUS.AVAILABLE:
                        sStatus = "Availble";
                        break;
                    case DEVICE_STATUS.AVAILABLE_CONNECTING:
                        sStatus = "Connecting...";
                        break;
                    case DEVICE_STATUS.AVAILABLE_CONNECTED:
                        sStatus = "Connected";
                        break;
                    case DEVICE_STATUS.AVAILABLE_MONITORING:
                        sStatus = "Connecting to Monitor Mode";
                        break;
                    case DEVICE_STATUS.AVAILABLE_MONITOR:
                        sStatus = "Monitoring...";
                        break;
                    //ERROR  > 1000 
                    case DEVICE_STATUS.NOT_SUPPORT:
                        sStatus = "Device is not support";
                        break;
                    //THONG TIN SAI
                    case DEVICE_STATUS.PORT_FORMAT_ERROR:
                        sStatus = "Port Device is not correct format";
                        break;
                    case DEVICE_STATUS.TYPE_FORMAT_ERROR:
                        sStatus = "Type Device is not correct format HR0253";
                        break;
                    case DEVICE_STATUS.IPADDRESS_ERROR_1:
                        sStatus = "IP Device is not correct format ";
                        break;
                    case DEVICE_STATUS.IPADDRESS_ERROR_2:
                        sStatus = "IP Device is not correct format ";
                        break;
                    case DEVICE_STATUS.IPADDRESS_ERROR_3:
                        sStatus = "IP Device is not correct format ";
                        break;
                    //LOI TOI LIEN QUAN TOI API
                    case DEVICE_STATUS.LOAD_API_ERROR:
                        sStatus = "API Device cannot load ";
                        break;
                    case DEVICE_STATUS.LOAD_API_CONTEXT_ERROR:
                        sStatus = "API context Device cannot load ";
                        break;
                    case DEVICE_STATUS.LOAD_API_INITIALIZA_ERROR:
                        sStatus = "API initializa Device cannot load ";
                        break;
                    case DEVICE_STATUS.LOAD_API_INITIALIZA_LISTENER_ERROR:
                        sStatus = "API initializa listener Device cannot load ";
                        break;
                    //LOI KET NOI 
                    case DEVICE_STATUS.CONNECT_DEVICE_ERROR:
                        sStatus = "Cannot connect Device";
                        break;
                    case DEVICE_STATUS.DEVICE_FOUND_ERROR:
                        sStatus = "Cannot found Device";
                        break;
                    case DEVICE_STATUS.SERVER_STOPED:
                        sStatus = "Server stoped";
                        break;
                }
                return sStatus;
            }
        }
        protected DEVICE_STATUS CheckInFoDevice()
        {
            DEVICE_STATUS statustmp = DEVICE_STATUS.AVAILABLE;
            try
            {
                int result = Int32.Parse(deviceInfo.Dev_Type);
            }
            catch
            {
                statustmp = DEVICE_STATUS.TYPE_FORMAT_ERROR;
            }

            if (statustmp != DEVICE_STATUS.AVAILABLE)
                return statustmp;


            //CHECK PORT
            try
            {
                int result = Int32.Parse(deviceInfo.Dev_Port);
            }
            catch
            {
                statustmp = DEVICE_STATUS.PORT_FORMAT_ERROR;
            }

            if (statustmp != DEVICE_STATUS.AVAILABLE)
                return statustmp;
            try
            {
                IPAddress ip = IPAddress.Parse(deviceInfo.Dev_Ip);
            }

            catch (ArgumentNullException e)
            {
                statustmp = DEVICE_STATUS.IPADDRESS_ERROR_1;
                DeviceLog(e.Message);
            }

            catch (FormatException e)
            {
                statustmp = DEVICE_STATUS.IPADDRESS_ERROR_2;
                DeviceLog(e.Message);
            }

            catch (Exception e)
            {
                statustmp = DEVICE_STATUS.IPADDRESS_ERROR_3;
                DeviceLog(e.Message);
            }


            return statustmp;
        }
        /*----------------------------------------------------------------------*/
        public void StartConnect()
        {
            if ((int)Status < 90000 || Status == DEVICE_STATUS.SERVER_STOPED)
            {
                //Thread.Sleep(100);
                //Thread t = new Thread(() => OnStartConnect());
                //t.Start();
                Task t = new Task(action, "StartConnect: "+ deviceInfo.Dev_Ip);
                t.Start();
            }
        }
        protected abstract void OnStartConnect();
        /*----------------------------------------------------------------------*/
        public abstract void StopConneted(bool skipStop = false);
      
        private void Timer_Tick(object sender, EventArgs e)
        {
            DeviceLog(((int)Status).ToString());
            int intStatus = (int)Status;
            if (intStatus > 90000)
            {
                //KHONG CAN LAM GI HET NHA
            }
            else if (intStatus > 10000)
            {
                StartConnect();
            }
            else
            {
                //if (Status == DEVICE_STATUS.AVAILABLE_MONITOR)
                //{
                    //GetDeviceInfo();

                    if ((int)status > 4)
                    {
                        RaiseStatusChange(status, StatusMessages);
                    }
                //}
            }
        }
        /*----------------------------------------------------------------------*/
    }
}
