﻿using GENUWIN_SDK_UNION.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GENUWIN_SDK_UNION.DeviceControl
{
    class UnKnowDevice:BaseDevice
    {
        public UnKnowDevice(DeviceInfo device) : base(device)
        {
            this.DeviceName = "Unknow";
        }

        public override void StopConneted(bool skipStop = false)
        {
            //throw new NotImplementedException();
        }

        protected override void OnStartConnect()
        {
           // throw new NotImplementedException();
        }

        //protected override void OnStartMonitoring()
        //{
        //    //throw new NotImplementedException();
        //}

        //protected override void OnStopMonitoring()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
