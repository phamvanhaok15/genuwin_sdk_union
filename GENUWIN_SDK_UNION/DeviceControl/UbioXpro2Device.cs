﻿using GENUWIN_SDK_UNION.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCBioBSPCOMLib;
using UCSAPICOMLib;

namespace GENUWIN_SDK_UNION.DeviceControl
{
    class UbioXpro2Device : BaseDevice
    {
        #region
        // UCSAPI
        public UCSAPICOMLib.UCSAPI ucsAPI;
        private IServerUserData serveruserData;
        private ITerminalUserData terminalUserData;
        private IServerAuthentication serverAuthentication;
        private IAccessLogData accessLogData;
        private ITerminalOption terminalOption;

        // UCBioBSP
        public UCBioBSPCOMLib.UCBioBSP ucBioBSP;
        public IFPData fpData;
        //public IFPImage testimg;
        //private ITemplateInfo templateInfo;
        public IDevice Idevice;
        public IExtraction extraction;
        public IFastSearch fastSearch;
        public IMatching matching;

        // initialize valiables member
        //public string szTextEnrolledFIR;
        //public byte[] binaryEnrolledFIR;
        public readonly long nTemplateType400 = 400;
        public readonly long nTemplateType800 = 800;
        public readonly long nTemplateType320 = 320;
        public readonly long nTemplateType256 = 256;
        //public string txtFilter;
        //public string txtStartDate, txtEndDate, txtStartTime, txtEndTime;
        //public string txtMessage;
        #endregion
        //custom
        private int Port = 0;

        public UbioXpro2Device(DeviceInfo device) : base(device)
        {
            this.DeviceName = "Ubio - X Pro2";
            Status = CheckInFoDevice();

            ucsAPI = new UCSAPIClass();
            serveruserData = ucsAPI.ServerUserData as IServerUserData;
            terminalUserData = ucsAPI.TerminalUserData as ITerminalUserData;
            accessLogData = ucsAPI.AccessLogData as IAccessLogData;
            serverAuthentication = ucsAPI.ServerAuthentication as IServerAuthentication;
            terminalOption = ucsAPI.TerminalOption as ITerminalOption;

            // create UCBioBSP Instance
            ucBioBSP = new UCBioBSPClass();
            fpData = ucBioBSP.FPData as IFPData;
            Idevice = this.ucBioBSP.Device as IDevice;
            extraction = this.ucBioBSP.Extraction as IExtraction;
            fastSearch = this.ucBioBSP.FastSearch as IFastSearch;
            matching = this.ucBioBSP.Matching as IMatching;

            ucsAPI.EventTerminalConnected += new _DIUCSAPIEvents_EventTerminalConnectedEventHandler(UCSCOMObj_EventTerminalConnected);
            ucsAPI.EventRealTimeAccessLog += new _DIUCSAPIEvents_EventRealTimeAccessLogEventHandler(ucsAPI_EventRealTimeAccessLog);
        }

        /*----------------------------------------------------------------------*/
        protected override void OnStartConnect()
        {
            

        }
        /*----------------------------------------------------------------------*/
        public override void StopConneted(bool skipStop = false)
        {
            ucsAPI.ServerStop();
            Status = DEVICE_STATUS.SERVER_STOPED;
            DeviceLog("stop server " + ucsAPI.ErrorCode.ToString("X4"));

        }
       
        void UCSCOMObj_EventTerminalConnected(int TerminalID, string TerminalIP)
        {
            DeviceLog("terminal connected"+TerminalID);
            //Status = DEVICE_STATUS.AVAILABLE_MONITOR;
            //DateTime t1 = DateTime.Now;
            //this.ucsAPI.SetTerminalTime((short)t1.Year, (byte)t1.Month, (byte)t1.Day, (byte)t1.Hour, (byte)t1.Minute, (byte)t1.Second);
            //RaiseLogEvent(
            //                    DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
            //                  , "Terminal Connected: " + TerminalID
            //                  , false
            //                  , null
            //                  , null
            //                  );
        }
        /////*----------------------------------------------------------------------*/
        ////protected override void OnStopMonitoring()
        ////{
        ////    ucsAPI.EventTerminalDisconnected += new _DIUCSAPIEvents_EventTerminalDisconnectedEventHandler(UCSCOMObj_EventTerminalDisconnected);
        ////}
        ////void UCSCOMObj_EventTerminalDisconnected(int TerminalID)
        ////{
        ////    DeviceLog("terminal disconnected");
        ////    Status = DEVICE_STATUS.AVAILABLE_CONNECTED;
        ////}
        /////*----------------------------------------------------------------------*/

  
        void ucsAPI_EventRealTimeAccessLog(int TerminalID)
        {
            RaiseLogEvent(
                           (this.accessLogData.DateTime.Substring(0, 11))
                          , this.accessLogData.UserID.ToString()
                          , false
                          , this.accessLogData.AuthMode.ToString()
                          , this.accessLogData.AuthMode.ToString()
                          );
        }
    }
}
